package io.github.ysedira.transactionsmanager;

import io.github.ysedira.transactionsmanager.domain.ErrorResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class TransactionsManagerExceptionHandler {

    @ExceptionHandler
    public ResponseEntity handleException(Exception e) {
        ErrorResponse errorResponse = ErrorResponse.of(e.getMessage());
        return ResponseEntity.badRequest()
                             .body(errorResponse);
    }
}
