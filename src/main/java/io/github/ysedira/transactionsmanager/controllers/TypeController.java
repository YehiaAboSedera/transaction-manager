package io.github.ysedira.transactionsmanager.controllers;

import io.github.ysedira.transactionsmanager.services.TransactionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping("/types")
public class TypeController {
    @Autowired
    private TransactionService service;

    @GetMapping("/{type-id}/transactions")
    Collection<Long> getTransactionsByType(@PathVariable(value = "type-id", required = true) String transactionType) {
        return service.findByType(transactionType);
    }

}
