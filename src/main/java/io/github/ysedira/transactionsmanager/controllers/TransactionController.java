package io.github.ysedira.transactionsmanager.controllers;

import io.github.ysedira.transactionsmanager.domain.Transaction;
import io.github.ysedira.transactionsmanager.domain.TransactionSum;
import io.github.ysedira.transactionsmanager.domain.exceptions.TransactionNotFoundException;
import io.github.ysedira.transactionsmanager.domain.TransactionOutcome;
import io.github.ysedira.transactionsmanager.services.TransactionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/transactions")
public class TransactionController {

    @Autowired
    private TransactionService service;

    @GetMapping("/{transaction-id}")
    Transaction getTransaction(@PathVariable(value = "transaction-id", required = true) Long transactionId) {
        return service.find(transactionId);
    }


    @GetMapping("/{transaction-id}/sum")
    TransactionSum getSum(@PathVariable(value = "transaction-id", required = true) Long transactionId) {
        return service.getSum(transactionId);
    }

    @PutMapping("/{transaction-id}")
    TransactionOutcome putTransaction(@PathVariable(value = "transaction-id", required = true) Long transactionId,
                                      @RequestBody(required = true) Transaction transaction) {
        transaction.setTransactionId(transactionId);
        return service.save(transaction);
    }

}
