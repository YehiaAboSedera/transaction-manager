package io.github.ysedira.transactionsmanager.services;

import io.github.ysedira.transactionsmanager.domain.*;
import io.github.ysedira.transactionsmanager.domain.exceptions.IllegalParentTransactionException;
import io.github.ysedira.transactionsmanager.domain.exceptions.TransactionNotFoundException;
import io.github.ysedira.transactionsmanager.repositories.TransactionRepository;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static io.github.ysedira.transactionsmanager.domain.TransactionStatus.OK;

@Service
public class TransactionService {
    @Autowired
    private TransactionRepository repository;

    public Transaction find(Long transactionId) {
        Optional<Transaction> optionalTransaction = repository.find(transactionId);
        if (optionalTransaction.isEmpty())
            throw new TransactionNotFoundException(transactionId);
        return optionalTransaction.get();
    }


    public Set<@NonNull Long> findByType(String transactionType) {
        return repository.findByType(transactionType)
                         .stream()
                         .map(Transaction::getTransactionId)
                         .collect(Collectors.toSet());
    }


    public TransactionSum getSum(Long transactionId) {
        var optionalTransaction = repository.find(transactionId);
        if (optionalTransaction.isEmpty()) {
            throw new TransactionNotFoundException(transactionId);
        }
        var transactionAmount = optionalTransaction.get()
                                                   .getAmount();
        Collection<Transaction> subTransactions = repository.getSubTransactions(transactionId);
        var sum = subTransactions
                .stream()
                .map(e -> getSum(e.getTransactionId()))
                .map(TransactionSum::getSum)
                .reduce(transactionAmount, Double::sum);

        return new TransactionSum(sum);
    }

    public TransactionOutcome save(Transaction transaction) {
        if (transaction.getTransactionId()
                       .equals(transaction.getParentId())) {
            throw new IllegalParentTransactionException();
        }
        repository.save(transaction);
        return new TransactionOutcome(OK);
    }

}
