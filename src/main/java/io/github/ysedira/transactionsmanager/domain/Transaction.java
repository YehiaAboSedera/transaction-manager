package io.github.ysedira.transactionsmanager.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Transaction {
    @JsonProperty(value ="transaction_id",required = false)
    private Long transactionId;
    private Double amount;
    private String type;
    @JsonProperty(value ="parent_id",required = false)
    private Long parentId;
}
