package io.github.ysedira.transactionsmanager.domain.exceptions;

public class TransactionNotFoundException extends RuntimeException {
    public TransactionNotFoundException(Long transactionId) {
        super(String.format("Unable to find Transaction with the following transaction Id [%d]", transactionId));
    }
}
