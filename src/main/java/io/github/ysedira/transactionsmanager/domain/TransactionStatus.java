package io.github.ysedira.transactionsmanager.domain;

public enum TransactionStatus {
    OK, ERROR
}
