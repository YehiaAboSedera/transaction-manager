package io.github.ysedira.transactionsmanager.repositories;

import io.github.ysedira.transactionsmanager.domain.Transaction;
import io.github.ysedira.transactionsmanager.domain.exceptions.TransactionNotFoundException;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Repository
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class InMemoryTransactionRepository implements TransactionRepository {

    private final ConcurrentHashMap<Long, Transaction> transactions;

    public InMemoryTransactionRepository() {
        transactions = new ConcurrentHashMap<>();
    }

    @Override
    public Optional<Transaction> find(Long transactionId) {
        if (transactions.containsKey(transactionId)) {
            return Optional.of(transactions.get(transactionId));
        }
        return Optional.empty();
    }

    @Override
    public Collection<Transaction> findByType(String transactionType) {
        return transactions.values()
                           .stream()
                           .filter(e -> e.getType()
                                         .equalsIgnoreCase(transactionType))
                           .collect(Collectors.toUnmodifiableList());
    }

    @Override
    public Collection<Transaction> getSubTransactions(Long transactionId) {
        return transactions.values()
                           .stream()
                           .filter(e -> transactionId.equals(e.getParentId()))
                           .collect(Collectors.toUnmodifiableList());
    }

    @Override
    public Transaction save(Transaction transaction) {
        if (transaction.getParentId() != null) {
            if (!transactions.containsKey(transaction.getParentId())) {
                throw new TransactionNotFoundException(transaction.getParentId());
            }
        }
        return transactions.put(transaction.getTransactionId(), transaction);
    }
}
