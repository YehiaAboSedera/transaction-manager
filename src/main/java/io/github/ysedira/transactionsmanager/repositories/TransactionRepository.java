package io.github.ysedira.transactionsmanager.repositories;

import io.github.ysedira.transactionsmanager.domain.Transaction;
import io.github.ysedira.transactionsmanager.domain.exceptions.TransactionNotFoundException;

import java.util.Collection;
import java.util.Optional;

public interface TransactionRepository {

    Optional<Transaction> find(Long transactionId);

    Collection<Transaction> findByType(String transactionType);

    Collection<Transaction> getSubTransactions(Long transactionId);

    Transaction save(Transaction transaction) throws TransactionNotFoundException;

}
