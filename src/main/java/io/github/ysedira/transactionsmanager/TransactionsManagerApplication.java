package io.github.ysedira.transactionsmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransactionsManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransactionsManagerApplication.class, args);
	}

}
