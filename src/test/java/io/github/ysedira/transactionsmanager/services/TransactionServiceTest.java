package io.github.ysedira.transactionsmanager.services;

import io.github.ysedira.transactionsmanager.domain.Transaction;
import io.github.ysedira.transactionsmanager.domain.TransactionOutcome;
import io.github.ysedira.transactionsmanager.domain.TransactionStatus;
import io.github.ysedira.transactionsmanager.domain.TransactionSum;
import io.github.ysedira.transactionsmanager.domain.exceptions.IllegalParentTransactionException;
import io.github.ysedira.transactionsmanager.domain.exceptions.TransactionNotFoundException;
import io.github.ysedira.transactionsmanager.repositories.TransactionRepository;
import org.assertj.core.util.Lists;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Set;

import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class TransactionServiceTest {
    private static final Long TRANSACTION_1_ID = 1234L;
    private static final Long TRANSACTION_2_ID = 1235L;
    private static final String TRANSACTION_TYPE_1 = "SOME_TYPE";
    private static final String TRANSACTION_TYPE_2 = "SOME_OTHER_TYPE";
    private static final Double TRANSACTION_AMOUNT = 1000.0;
    private static final Transaction TRANSACTION_1 = new Transaction(TRANSACTION_1_ID, TRANSACTION_AMOUNT, TRANSACTION_TYPE_1, null);
    private static final Transaction TRANSACTION_2 = new Transaction(TRANSACTION_2_ID, TRANSACTION_AMOUNT, TRANSACTION_TYPE_2, TRANSACTION_1_ID);
    private static final ArrayList<Transaction> TRANSACTIONS = Lists.list(TRANSACTION_1);

    private static final String INVALID_TRANSACTION_TYPE = "INVALID_TYPE";
    private static final Transaction INVALID_TRANSACTION = new Transaction(TRANSACTION_2_ID, TRANSACTION_AMOUNT, TRANSACTION_TYPE_2, TRANSACTION_2_ID);
    @MockBean
    TransactionRepository repository;

    @Autowired
    TransactionService service;

    @Before
    public void setUp() {
        reset(repository);
    }

    @Test
    public void testFindExistingTransactionSuccessfully() {
        when(repository.find(TRANSACTION_1_ID)).thenReturn(Optional.of(TRANSACTION_1));

        Transaction transaction = service.find(TRANSACTION_1_ID);

        Assert.assertEquals(TRANSACTION_1, transaction);

        verify(repository).find(TRANSACTION_1_ID);
    }

    @Test(expected = TransactionNotFoundException.class)
    public void testFindThrowWhenTransactionIsNotFound() {
        when(repository.find(TRANSACTION_1_ID)).thenReturn(Optional.empty());

        service.find(TRANSACTION_1_ID);

        verify(repository).find(TRANSACTION_1_ID);
    }

    @Test
    public void testFindByTypeReturnListOfTransactionMatchingTheGivenType() {

        when(repository.findByType(TRANSACTION_TYPE_1)).thenReturn(TRANSACTIONS);

        Set<Long> transactionIds = service.findByType(TRANSACTION_TYPE_1);
        Assert.assertEquals(1, transactionIds.size());
        Assert.assertTrue(transactionIds.contains(TRANSACTION_1_ID));

        verify(repository).findByType(TRANSACTION_TYPE_1);
    }

    @Test
    public void testFindByTypeReturnEmptyListIfNoTransactionMatchTheGivenType() {

        when(repository.findByType(INVALID_TRANSACTION_TYPE)).thenReturn(Lists.emptyList());

        Set<Long> transactionIds = service.findByType(INVALID_TRANSACTION_TYPE);
        Assert.assertEquals(0, transactionIds.size());

        verify(repository).findByType(INVALID_TRANSACTION_TYPE);
    }

    @Test
    public void testGetSumReturnTheTotalAmountOfTransactionWithoutChild() {
        when(repository.find(TRANSACTION_1_ID)).thenReturn(Optional.of(TRANSACTION_1));
        when(repository.getSubTransactions(TRANSACTION_1_ID)).thenReturn(Lists.emptyList());

        TransactionSum sum = service.getSum(TRANSACTION_1_ID);
        Assert.assertEquals(TRANSACTION_AMOUNT, sum.getSum());

        verify(repository).getSubTransactions(TRANSACTION_1_ID);
        verify(repository).find(TRANSACTION_1_ID);
    }

    @Test
    public void testGetSumReturnTheTotalAmountOfTransactionWithChild() {
        when(repository.find(TRANSACTION_1_ID)).thenReturn(Optional.of(TRANSACTION_1));
        when(repository.find(TRANSACTION_2_ID)).thenReturn(Optional.of(TRANSACTION_2));
        when(repository.getSubTransactions(TRANSACTION_1_ID)).thenReturn(Lists.list(TRANSACTION_2));
        when(repository.getSubTransactions(TRANSACTION_2_ID)).thenReturn(Lists.emptyList());

        TransactionSum sum = service.getSum(TRANSACTION_1_ID);
        Assert.assertEquals(sum.getSum(), TRANSACTION_AMOUNT * 2.0, 0.0);

        verify(repository).getSubTransactions(TRANSACTION_1_ID);
        verify(repository).find(TRANSACTION_1_ID);
        verify(repository).getSubTransactions(TRANSACTION_2_ID);
        verify(repository).find(TRANSACTION_2_ID);
    }

    @Test
    public void testSaveTransactionSuccessfully() {
        when(repository.save(TRANSACTION_1)).thenReturn(TRANSACTION_1);

        TransactionOutcome outcome = service.save(TRANSACTION_1);
        Assert.assertEquals(TransactionStatus.OK, outcome.getStatus());

        verify(repository).save(TRANSACTION_1);
    }

    @Test(expected = IllegalParentTransactionException.class)
    public void testSaveThrowAnExceptionWhenTransactionIdAndParentIdAreEqual() {
        service.save(INVALID_TRANSACTION);
    }
}