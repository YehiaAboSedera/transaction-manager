# Transaction Manager

## Assumption
* Transactions don't have cycles, for instance, given three transactions: A, B, and C.
     if A is the parent B, and B is the parent of C, C cannot be the parent of A.

## Running The App

```bash
mvn spring-boot:run
```

## Running Tests

```bash
mvn test
```

## APIs 

Create Or update transaction
```bash
curl --request PUT \
     --url http://localhost:8080/transactions/{transaction-id} \	 
	 --data '{
	            "amount":double,
	            "type":"string",
	            "parent_id":"long",
             }' \
	 --header 'Content-type: application/json'
```
Get Transaction
```bash
curl --request GET \
     --url http://localhost:8080/transactions/{transaction-id}
```
Get Transaction Sum 
```bash
curl --request GET \
     --url http://localhost:8080/transactions/{transaction-id}/sum
```

Get Transaction By Type 
```bash
curl --request GET \
     --url http://localhost:8080/types/{type}/transactions
 ```
     
## Performance Analysis

- Save and Get by Id are O(1), as transactions are stored in HashMap, which guarantee a constant time access.
- Get By Type is O(N) as a scan of all transaction is needed inorder to filter transaction fitting the criteria.
- Get Sum is O(B^d), depth-first search is used to traverse and extract all the sub-transactions. The current 
    implementation is very expensive as in every level as full scan is performed to get the sub-transaction. 
    This can be optimized by introducing an index or add a list of sub-transactions in each transaction.
